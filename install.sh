#!/bin/bash
export PASS='password here'
xcode-select --install
echo $PASS | sudo -S xcodebuild -license accept
rm -rf ~/.config

# clone
git clone https://github.com/powerline/fonts.git --depth=1
# install
cd fonts
mkdir ~/.config
mkdir ~/.config/fontconfig
cp fontconfig/50-enable-terminess-powerline.conf ~/.config/fontconfig/conf.d
./install.sh

# clean-up a bit
rm -rf fonts
cd ..

$(pwd)/homebrew/install.sh
$(pwd)/zsh/install.sh
$(pwd)/tmux/install.sh
$(pwd)/nvm/install.sh
$(pwd)/vim/install.sh
echo $PASS | sudo -S launchctl load -w /System/Library/LaunchDaemons/com.apple.locate.plist
unset PASS
cd ~/.vim/bundle/tern_for_vim
npm install
cd ~
npm install -g yarn
yarn global add gulp prettier jest webpack yo eslint mocha shx wikit how2
source ~/.zshrc
echo Ready to rock and roll!

