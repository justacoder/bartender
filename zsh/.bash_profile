export PATH="/usr/local/sbin:$PATH"
export NVM_DIR=~/.nvm
export CLICOLOR=1;
ZSHA_BASE=$HOME/.antigen
source $(pwd)/powerlevel9k.sh

source $(brew --prefix nvm)/nvm.sh
source $(brew --prefix)/share/antigen/antigen.zsh
# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle pip
antigen bundle lein
antigen bundle command-not-found
# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting
# Load the theme.
#antigen theme https://github.com/denysdovhan/spaceship-zsh-theme spaceship
antigen theme bhilburn/powerlevel9k powerlevel9k
# Tell Antigen that you're done.
antigen apply

alias getwr='curl -4 http://wttr.in/\~nyc'

alias gs='git status '
alias ga='git add '
alias gb='git branch '
alias gc='git commit'
alias gd='git diff'
alias go='git checkout '
alias gk='gitk --all&'
alias gx='gitx --all'
alias got='git '
alias get='git '
