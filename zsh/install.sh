#!/bin/bash
rm -f ~/.bash_profile
rm -f ~/.zshrc

ln -s $(pwd)/zsh/.zshrc ~/.zshrc
ln -s $(pwd)/zsh/.bash_profile ~/.bash_profile

echo $PASS | sudo -S dscl . -create /Users/$USER UserShell $(which zsh)
echo zsh installation finished

source ~/.zshrc
