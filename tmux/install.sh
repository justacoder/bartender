#!/bin/bash
rm -f ~/.tmux.conf
rm -rf ~/.tmux/plugins/*
ln -s $(pwd)/tmux/.tmux.conf ~/.tmux.conf
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
tmux source ~/.tmux.conf

echo tmux installation finished.

