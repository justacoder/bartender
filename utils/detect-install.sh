#!/bin/bash
binary=$(which $1)
if [ ! $binary == '' ] && [ -x $binary ]
then
  echo 1
else
  echo 0
fi

