#!/bin/bash

function findCurrentShellType {
  if [ "$version" ]
    then
      echo 'tcsh';
  elif [ "$ZSH" ]
    then
      echo 'zsh';
  elif [ "$BASH" ]
    then
      echo 'bash';
  elif [ "$shell" ]
    then
      echo $shell;
  fi

  return 0
}

findCurrentShellType
