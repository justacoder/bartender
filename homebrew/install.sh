#!/bin/bash
binary=$(which brew)
if [ ! $binary == '' ] && [ -x $binary ]
then
  echo A homebrew installation found.
else
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

rm -f ~/Brewfile
ln -s $(pwd)/homebrew/Brewfile ~/Brewfile

echo updating...
brew update

echo upgrading...
brew upgrade

echo running prune...
brew prune

echo running cleanup...
brew cleanup

echo running doctor...
brew doctor

echo installing formulas...
project_dir=$(pwd)
cd ~/
brew bundle
cd $project_dir

echo formula installations finished.
