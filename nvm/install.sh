#!/bin/bash
rm -f ~/.nvmrc
rm -rf ~/.nvm
mkdir ~/.nvm
source $(brew --prefix nvm)/nvm.sh
echo "lts/carbon" >> ~/.nvmrc

nvm install lts/argon
nvm install lts/boron
nvm install lts/carbon
nvm install node
current_folder=$(pwd)
cd ~/
nvm use
cd $current_folder
echo node version installations finished.


