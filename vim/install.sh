#!/bin/bash
rm -f ~/.vimrc
rm -rf ~/.vim
ln -s $(pwd)/vim/.vimrc ~/.vimrc
mkdir ~/.vim
mkdir ~/.vim/bundle
mkdir ~/.vim/tmp
mkdir ~/.vim/tmp/undo
mkdir ~/.vim/tmp/backup
mkdir ~/.vim/tmp/swap
mkdir ~/.vim/tmp/tags

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
git clone https://github.com/morhetz/gruvbox.git ~/.vim/bundle/gruvbox

vim +PluginInstall +qall
source ~/.vim/bundle/gruvbox/gruvbox_256palette_osx.sh

echo vim installation finished.

