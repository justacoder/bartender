set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'L9'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'scrooloose/nerdtree'
Plugin 'morhetz/gruvbox'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'rdnetto/YCM-Generator'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'tpope/vim-fugitive'
Plugin 'bling/vim-airline'
Plugin 'airblade/vim-gitgutter'
Plugin 'altercation/vim-colors-solarized'
Plugin 'flazz/vim-colorschemes'
Plugin 'elzr/vim-json'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'mileszs/ack.vim'
Plugin 'tyok/nerdtree-ack'
Plugin 'rking/ag.vim'
Plugin 'bronson/vim-trailing-whitespace'
Plugin 'embear/vim-localvimrc'
Plugin 'majutsushi/tagbar'
Plugin 'taglist.vim'
"Plugin 'Valloric/YouCompleteMe'
Plugin 'tikhomirov/vim-glsl'
Plugin 'sbdchd/neoformat'
Plugin 'prettier/prettier'
Plugin 'mitermayer/vim-prettier'
Plugin 'easymotion/vim-easymotion'
Plugin 'bash-support.vim'
Plugin 'groenewege/vim-less'
Plugin 'SirVer/ultisnips'
Plugin 'sjl/gundo.vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'tpope/vim-surround'
Plugin 'jeffkreeftmeijer/vim-numbertoggle'
Plugin 'scrooloose/nerdcommenter'
Plugin 'ervandew/supertab'
Plugin 'mxw/vim-jsx'
Plugin 'ivalkeen/vim-simpledb'
Plugin 'lifepillar/pgsql.vim'
Plugin 'rizzatti/dash.vim'
Plugin 'derekwyatt/vim-scala'
Plugin 'marijnh/tern_for_vim'
Plugin 'jnurmine/zenburn'
Plugin 'rust-lang/rust.vim'
Plugin 'cespare/vim-toml'

call vundle#end()
filetype plugin indent on

autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
autocmd BufNewFile,BufFilePre,BufRead *.cql set filetype=sql
autocmd BufNewFile,BufFilePre,BufRead *.mjs set filetype=javascript
autocmd QuickFixCmdPost *grep* cwindow
autocmd QuickFixCmdPost *glog* cwindow
autocmd BufReadPost fugitive://* set bufhidden=delete

set backspace=indent,eol,start
set list listchars=tab:»·,trail:·,eol:¬,nbsp:«
set showcmd
set number
set relativenumber
set wildmenu
set lazyredraw
set scrolloff=3
set cursorline
set mat=2
let mapleader = ','
set autoread
set laststatus=2
set noeb vb t_vb=
set smartcase
set incsearch
set tabstop=2
set expandtab
set shiftwidth=2
set softtabstop=2
set nofoldenable
set foldmethod=syntax
set diffopt+=vertical
set splitright
set splitbelow
set suffixesadd+=.js,.jsx,.json,.es6,.es,.es2015,.css,.scss,.h,.c,.cpp,.hpp
set statusline+=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P
set statusline+=%*
set t_Co=256
set background=dark
set undodir=~/.vim/tmp/undo/
set backupdir=~/.vim/tmp/backup/
set directory=~/.vim/tmp/swap/
set tags=~/.vim/tmp/tags
set tags=tags;/
set clipboard=unnamed

let g:EasyMotion_do_mapping = 0
let g:EasyMotion_smartcase = 1
let g:solarized_termcolors=256
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:localvimrc_name=[ ".vimrc.local" ]
let g:localvimrc_ask=0
let g:ctrlp_show_hidden = 1
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:prettier#exec_cmd_path = "$(which prettier)"

colorscheme gruvbox
syntax enable

nnoremap <C-e> :NERDTreeToggle<cr>
nnoremap <C-f> :NERDTreeFind<cr>
nnoremap <silent> <F9> :TagbarToggle<CR>
nnoremap gp :silent %!prettier --stdin --config ~/code/branded/js/xfe/.prettierrc<CR>
noremap ;' :%s:::g<Left><Left><Left>
noremap ;; :%s:::cg<Left><Left><Left><Left>
nmap <Leader>py <Plug>(Prettier)
nnoremap <C-e> :NERDTreeToggle<cr>
nnoremap <C-f> :NERDTreeFind<cr>
nnoremap gV `[v`] " select las insert content
inoremap kj <esc>
inoremap jk <esc>
inoremap jj <esc>
inoremap kk <esc>
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l
nnoremap Q <nop>
nnoremap Gf <C-w>vgf
vnoremap >> >gv
vnoremap << <gv
nnoremap <F5> :GundoToggle<CR>
nmap s <Plug>(easymotion-overwin-f)
map ssj <Plug>(easymotion-j)
map ssk <Plug>(easymotion-k)

"augroup NeoformatAutoFormat
    "autocmd!
    "autocmd FileType javascript,javascript.jsx,javascript.mjs setlocal formatprg=prettier\
                                                            "\--stdin\
                                                            "\--config\ ~/code/branded/js/xfe/.prettierrc\
                                                            "\--single-quote\
                                                            "\--trailing-comma\ es5
    "autocmd BufWritePre *.js,*.jsx,*.mjs Neoformat
"augroup END
